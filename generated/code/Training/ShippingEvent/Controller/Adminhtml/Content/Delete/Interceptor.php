<?php
namespace Training\ShippingEvent\Controller\Adminhtml\Content\Delete;

/**
 * Interceptor class for @see \Training\ShippingEvent\Controller\Adminhtml\Content\Delete
 */
class Interceptor extends \Training\ShippingEvent\Controller\Adminhtml\Content\Delete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Training\ShippingEvent\Model\EventFactory $shippingEventModelFactory)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $shippingEventModelFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
