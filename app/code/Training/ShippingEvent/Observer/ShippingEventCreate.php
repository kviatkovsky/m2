<?php

namespace Training\ShippingEvent\Observer;

use Magento\Framework\Event\ObserverInterface;
use Training\ShippingEvent\Model\EventFactory as ShippingEventFactory;

class ShippingEventCreate implements ObserverInterface
{

    protected $_eventFactory;

    public function __construct(
        ShippingEventFactory $eventFactory
    ) {
        $this->_eventFactory    = $eventFactory;
    }


    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /**
         * @var $product \Magento\Catalog\Api\Data\ProductInterface
         */
        $product        = $observer->getProduct();
        $qty            = $product->getQty();
        $price          = $product->getFinalPrice();
        $totalAmount    = $qty*$price;
        $eventFactory   = $this->_eventFactory->create();
        $eventFactory->setBaseTotalAmount($totalAmount);
        $eventFactory->save();
    }

}
