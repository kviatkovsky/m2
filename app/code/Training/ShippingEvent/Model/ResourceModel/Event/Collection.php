<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\ShippingEvent\Model\ResourceModel\Event;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * CMS Block Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'event_id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'shipping_event_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'shipping_event_collection';


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\ShippingEvent\Model\Event::class, \Training\ShippingEvent\Model\ResourceModel\Event::class);
    }

}
