<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\ShippingEvent\Api\Data;

/**
 * CMS block interface.
 * @api
 * @since 100.0.2
 */
interface EventInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const EVENT_ID      = 'event_id';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME   = 'update_time';
    const IS_ACTIVE     = 'is_active';
    const TOTAL_AMOUNT  = 'base_total_amount';
    const ORDER_ID      = 'order_id';
    const COMMENT       = 'comment';
    const VISIBILITY    = 'visibility';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get content
     *
     * @return string|null
     */
    public function getBaseTotalAmount();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Get Order ID
     *
     * @return string|null
     */
    public function getOrderId();

    /**
     * Get Comment
     *
     * @return string|null
     */
    public function getComment();

    /**
     * Get Visibility
     *
     * @return integer|null
     */
    public function getVisibility();

    /**
     * Set ID
     *
     * @param $id
     * @return $this
     */
    public function setId($id);

    /**
     * Set Base Total Amount
     *
     * @param int $total
     * @return $this
     */
    public function setBaseTotalAmount($total);

    /**
     * Set Creation Time
     *
     * @param $time
     * @return $this
     */
    public function setCreationTime($time);

    /**
     * Set Update Time
     *
     * @param $time
     * @return $this
     */
    public function setUpdateTime($time);

    /**
     * Set Order ID
     *
     * @param $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * Set Comment
     *
     * @param $comment
     * @return $this
     */
    public function setComment($comment);

    /**
     * Set Visibility
     *
     * @param $visibility
     * @return $this
     */
    public function setVisibility($visibility);

}
